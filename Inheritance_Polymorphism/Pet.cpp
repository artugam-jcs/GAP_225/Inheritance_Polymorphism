#include <iostream>

#include "Pet.h"

Pet::Pet(std::string name, int nutrition, int happiness)
	: m_name(name)
	, m_description("")
	, m_nutrition(nutrition)
	, m_happiness(happiness)
{
	// ..
}

void Pet::Print()
{
	std::cout << std::endl;
	std::cout << "Name: " << m_name << std::endl;
	std::cout << "Description: " << m_description << std::endl;
	std::cout << "Nutrition: " << m_nutrition << std::endl;
	std::cout << "Happiness: " << m_happiness << std::endl;
	std::cout << std::endl;
}
