#include <iostream>

#include "Util.h"

#include "Cat.h"
#include "Dog.h"

#include "Player.h"

Player::Player()
	: m_pets()
	, m_numPets(0)
{

}

Player::~Player()
{
	Free(m_pets);
}

bool Player::ChooseAction()
{
	int action;

	system("cls");

	std::cout << "What you want to do?" << std::endl;
	std::cout << "0) Quit" << std::endl;
	std::cout << "1) Adopt a new pet" << std::endl;

	if (m_pets.capacity() != 0)
	{
		std::cout << "2) Feed pets" << std::endl;
		std::cout << "3) Cuddle pets" << std::endl;
	}

	std::cout << "Type your answer: (number) ";
	std::cin >> action;

	switch (action)
	{
	case 0: 
		return false;
	case 1: 
		Adopt(); 
		break;
	case 2: 
		FeedPet();
		break;
	case 3: 
		CuddlePet();
		break;
	default:
		std::cout << "Failed to choose action: " << action << std::endl;
		break;
	}

	UpdatePets();
	PrintPets();

	system("pause");

	return true;
}

void Player::Adopt()
{
	std::string type;

	std::cout << "Dog or Cat: ";
	std::cin >> type;

	if (!IsDog(type) && !IsCat(type))
	{
		std::cout << "Invalid answer: " << type << std::endl;
		return;
	}

	Pet* pNewPet;

	std::string name;
	std::cout << "Name your new pet: ";
	std::cin >> name;

	if (IsDog(type))
	{
		pNewPet = new Dog(name, 50, 50);
	}
	else
	{
		pNewPet = new Cat(name, 50, 50);
	}

	m_pets.emplace_back(pNewPet);
}

void Player::FeedPet()
{
	std::vector<Pet*>::iterator iter = m_pets.begin();

	for (Pet* pet : m_pets)
	{
		pet->Feed();
	}
}

void Player::CuddlePet()
{
	std::vector<Pet*>::iterator iter = m_pets.begin();

	for (Pet* pet : m_pets)
	{
		pet->Cuddle();
	}
}

void Player::UpdatePets()
{

}

void Player::PrintPets()
{
	std::cout << std::endl;

	std::vector<Pet*>::iterator iter = m_pets.begin();

	for (Pet* pet : m_pets)
	{
		pet->Print();
	}
}


bool Player::IsDog(std::string& type)
{
	return type == "dog" || type == "Dog" || type == "DOG";
}

bool Player::IsCat(std::string& type)
{
	return type == "cat" || type == "Cat" || type == "CAT";
}
