#include <iostream>

#include "Game.h"

/**
 * @brief Program entry
 */
int main()
{
	srand(time(NULL));

	Game game;

	game.Start();

	return 0;
}
