#include "Game.h"

#include "Player.h"

Game::Game()
{
	m_pPlayer = new Player();
}

Game::~Game()
{
	delete m_pPlayer;
}

void Game::Start()
{
	while (m_pPlayer->ChooseAction()) ;
}
