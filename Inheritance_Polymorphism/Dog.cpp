#include <iostream>

#include "Util.h"
#include "Dog.h"

Dog::Dog(std::string name, int nutrition, int happiness)
	: Pet(name, nutrition, happiness)
{
	Randomize();
}

void Dog::Randomize()
{
	if (m_description != "")
		return;

	m_description = MakeChoice(std::vector<std::string>
	{
		"Bulldog", "Golden Retriever", "Siberian Husky"
	});

	std::cout << "Breed: " << m_description;
}

void Dog::Feed()
{
	std::cout << "You feed " << m_name << " some dog chow. They love it!" << std::endl;

	--m_happiness;
	--m_nutrition;
}

void Dog::Cuddle()
{
	m_happiness += m_happiness / 2;

	--m_happiness;
	--m_nutrition;
}
