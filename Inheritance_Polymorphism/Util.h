#pragma once

#include <cstdarg>
#include <random>

inline int Random(int min, int max)
{
	return rand() % max + min;
}

inline int IsPossible(int chance)
{
	int roll = Random(1, 100);
	return roll >= chance;
}

inline std::string MakeChoice(std::vector<std::string> list)
{
	int choice = Random(0, (int)list.capacity());
	return list.at(choice);
}

template<class T>
inline void Free(std::vector<T*> list)
{
	for (auto elm : list) delete elm;
	list.clear();
}
