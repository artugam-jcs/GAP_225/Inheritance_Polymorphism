#pragma once

#include "Pet.h"

class Cat : public Pet
{
public:
	Cat(std::string name, int nutrition, int happiness);

	virtual void Randomize() override;
	virtual void Feed() override;
	virtual void Cuddle() override;
};

