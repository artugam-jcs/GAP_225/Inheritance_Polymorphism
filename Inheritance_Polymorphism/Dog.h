#pragma once

#include "Pet.h"

class Dog : public Pet
{
public:
	Dog(std::string name, int nutrition, int happiness);

	virtual void Randomize() override;
	virtual void Feed() override;
	virtual void Cuddle() override;
};

