#pragma once

#include <string>
#include <vector>

class Pet;

class Player
{
private:
	std::vector<Pet*> m_pets;
	int m_numPets;

public:
	Player();
	~Player();

	bool ChooseAction();
	void Adopt();
	void FeedPet();
	void CuddlePet();
	void UpdatePets();
	void PrintPets();

private:
	bool IsDog(std::string& type);
	bool IsCat(std::string& type);
};
