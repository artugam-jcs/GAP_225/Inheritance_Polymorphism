#pragma once

#include <string>

class Pet
{
protected:
	std::string m_name;
	std::string m_description;
	int m_nutrition;
	int m_happiness;

public:
	Pet(std::string name, int nutrition, int happiness);

	virtual void Randomize() = 0;
	virtual void Feed() = 0;
	virtual void Cuddle() = 0;
	void Print();
};

