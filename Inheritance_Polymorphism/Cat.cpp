#include <iostream>

#include "Util.h"
#include "Cat.h"

Cat::Cat(std::string name, int nutrition, int happiness)
	: Pet(name, nutrition, happiness)
{
	Randomize();
}

void Cat::Randomize()
{
	if (m_description != "")
		return;

	m_description = MakeChoice(std::vector<std::string>
	{
		"Black cat", "Tabby cat", "Orange cat"
	});

	std::cout << "Breed: " << m_description;
}

void Cat::Feed()
{
	std::cout << "You feed " << m_name << " some tuna. They love it!" << std::endl;
	++m_nutrition;
}

void Cat::Cuddle()
{
	bool happy = IsPossible(60);

	if (happy)
		m_happiness = 100;
	else
		m_happiness -= 5;
}
